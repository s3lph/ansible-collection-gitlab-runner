#!/usr/bin/python

# Copyright (c) 2022 s3lph
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice (including the
# next paragraph) shall be included in all copies or substantial
# portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


DOCUMENTATION = r'''
---
module: runner

short_description: Register and manage gitlab-runner runner instances.

version_added: "0.1.0"

description: Register and manage gitlab-runner runner instances.

options:
    name:
        description: Name (=description) of the runner instance.
        type: str
        required: yes
    exec:
        description: Path to the gitlab-runner binary.
        type: str
        default: /usr/bin/gitlab-runner
    file:
        description: Path to the gitlab-runner config file.
        type: str
        default: /etc/gitlab-runner/config.toml
    url:
        description: Gitlab server base URL.
        type: str
        default: https://gitlab.com
    token:
        description: Registration token issued by the Gitlab server.
        type: str
    state:
        type: str
        default: present
        choices: [ absent, present, update ]

author:
    - s3lph <account-gitlab-ideynizv@kernelpanic.lol>
'''

EXAMPLES = r'''

# Register a runner instance
- name: register runner01
  s3lph.gitlab_runner.runner:
    name: runner01
    url: https://gitlab.example.com
    token: 0wDlhqJlrMzyBB8v

# Remove a runner instance
- name: remove runner01
  s3lph.gitlab_runner.runner:
    name: runner01
    state: absent
'''

RETURN = r'''
# These are examples of possible return values, and in general should use other names for return values.
original_message:
    description: The original name param that was passed in.
    type: str
    returned: always
    sample: 'hello world'
message:
    description: The output message that the test module generates.
    type: str
    returned: always
    sample: 'goodbye'
'''

from ansible.module_utils.basic import AnsibleModule

import toml
import subprocess
import shlex


def parse_config_toml(filename, runner_name):
    with open(filename, 'r') as f:
        c = toml.load(f)
    for r in c['runners']:
        if r['name'] == runner_name:
            return r
    return None


def register_runner(module, existing_runner, result):
    if existing_runner is not None:
        return
    result['changed'] = True
    cmd = [
        module.params['exec'],
        'register',
        '--non-interactive',
        '--config', module.params['file'],
        '--name', module.params['name'],
        '--url', module.params['url'],
        '--registration-token', module.params['token'],
        '--executor', module.params['executor']
    ]
    if module.params['tags']:
        cmd.extend([
            '--tag-list',
            ','.join(module.params['tags']) if isinstance(module.params['tags'], list) else module.params['tags']
        ])
    if module.params['maintenance_note']:
        cmd.extend(['--maintenance-note', module.params['maintenance_note']])
    if module.params['limit']:
        cmd.extend(['--limit', str(module.params['limit'])])
    # Convert e.g.:
    #     docker:
    #       image: debian:bullseye
    #       privileged: true
    # to:
    #     --docker-image debian:bullseye --docker-privileged
    for k, v in module.params[module.params['executor']]:
        arg = '--{}-{}'.format(module.params['executor'], k.replace('_', '-'))
        if isinstance(v, bool):
            cmd.append(arg)
        else:
            cmd.extend([arg, str(v)])

    result['cmd'] = shlex.join(cmd)
    if module.check_mode:
        return
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result['stdout'], result['stderr'] = p.communicate()
    result['rc'] = p.returncode


def deregister_runner(module, existing_runner, result):
    if existing_runner is None:
        return
    result['changed'] = True
    cmd = [
        module.params['exec'],
        'unregister',
        '--config', module.params['file'],
        '--name', module.params['name'],
    ]

    result['cmd'] = shlex.join(cmd)
    if module.check_mode:
        return
    p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result['stdout'], result['stderr'] = p.communicate()
    result['rc'] = p.returncode


def run_module():
    # define available arguments/parameters a user can pass to the module
    module_args = dict(
        exec=dict(type='str', required=False, default='/usr/bin/gitlab-runner'),
        file=dict(type='str', required=False, default='/etc/gitlab-runner/config.toml'),
        name=dict(type='str', required=True),
        url=dict(type='str', required=False, default='https://gitlab.com'),
        token=dict(type='str', required=False),
        state=dict(type='str', required=False, default='present', choices=['absent', 'present', 'update']),
        tags=dict(type='list', elements='str', required=False, default=[]),
        limit=dict(type='int', required=False),
        maintenance_note=dict(type='str', required=False),
        executor=dict(type='str', required=False, default='docker'),
        docker=dict(type='dict', required=False, default={}),
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # changed is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    runner = parse_config_toml(module.params['file'], module.params['name'])

    if module.params['state'] == 'absent' or module.params['state'] == 'update':
        deregister_runner(module, runner, result)
        runner = parse_config_toml(module.params['file'], module.params['name'])

    if module.params['state'] == 'present' or module.params['state'] == 'update':
        register_runner(module, runner, result)
        runner = parse_config_toml(module.params['file'], module.params['name'])

    if runner:
        del runner['token']
        result['runner'] = runner

    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
